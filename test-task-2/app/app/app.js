/**
 * Created by dunice on 04.02.16.
 */
'use strict';

angular.module('myApp.mainApp', ['ngRoute', 'angularFileUpload'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/app', {
            templateUrl: 'app/app.html',
            controller: 'mainAppCtrl'
        });
    }])

    .controller('mainAppCtrl', ['$scope', 'FileUploader', function($scope, FileUploader) {
        var uploader = $scope.uploader = new FileUploader({
            url: 'upload.php'
        });
        $scope.images = [];
        $scope.messages = [];

        $scope.closeMessage = closeMessage;

        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            $scope.messages.push({
                type: "danger",
                id: new Date().getTime(),
                item: item
            });
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            $scope.messages.push({
                type: "danger",
                id: new Date().getTime(),
                item: fileItem.file
            });
        };

        function closeMessage(errorMessage) {
            var newMessages = [];

            angular.forEach($scope.messages, function (error) {
                if (errorMessage.id !== error.id) {
                    newMessages.push(error);
                }
            });
            $scope.messages = newMessages;
        }
    }]);