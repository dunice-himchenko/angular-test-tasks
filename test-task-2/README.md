# Run process

### Install with apache or nginx

0. Copy *test task 2* directory to _/var/www/html/_
1. Install *bower components*
```
bower install
```

2. open [localhost](http://localhost/test%20task%2/app/index.html) in your browser


### Install process with python
0. Open in terminal directory:
```
~/path/to/project/ $ cd test\ task\ 2/
```
1. Run *python HTTP server*
```
~/path/to/project/test task 2 $ python im SimpleHTTPServer
```
2. Open [localhost](http://localhost:8000/app/index.html) in your browser
