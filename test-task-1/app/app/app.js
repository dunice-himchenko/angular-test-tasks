/**
 * Created by dunice on 04.02.16.
 */
'use strict';

angular.module('myApp.mainApp', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/app', {
            templateUrl: 'app/app.html',
            controller: 'mainAppCtrl'
        });
    }])

    .controller('mainAppCtrl', ['$scope', '$http', function($scope, $http) {
        var emptyTreatement = {
            treatmentName: ""
            , treatmentId: 0
            , cost: 0
            , status: "-select-"
            , note: ""
            , showNote: false
        }
        ;

        $scope.localData = [];
        $http({
            method: "GET",
            url: "http://dct4avjn1lfw.cloudfront.net/doctors/listDoctorProcedure/85"
        }).then(function(response) {
            $scope.localData = response.data;
        }, function(response) {});

        $scope.newMuscleTreatement = angular.copy(emptyTreatement);
        $scope.newJointTreatement = angular.copy(emptyTreatement);

        $scope.muscleTreatements = [];
        $scope.jointTreatements = [];

        $scope.addTreatement = addTreatement;
        $scope.remove = remove;
        $scope.selectTreatement = selectTreatement;
        $scope.changedCb = autocompleteChangedCb;
        $scope.highlightOutput = highlightOutput;
        $scope.finalOutput = "";



        function selectTreatement(selectedTreatement) {
            if (!selectedTreatement) {
                return void 0;
            }

            if (this.id === "newMuscleTreatement" || this.id === "newJointTreatement") {
                angular.extend($scope[this.id], {
                    treatmentId: selectedTreatement.originalObject.ProcedureID
                    , treatmentName: selectedTreatement.originalObject.ProcedureName
                    , cost: selectedTreatement.originalObject.ProcedureCost
                    , note: selectedTreatement.originalObject.DefaultNotes
                });

                return void 0;
            }

            angular.extend($scope[this.id][this.$parent.$index], {
                treatmentId: selectedTreatement.originalObject.ProcedureID
                , treatmentName: selectedTreatement.originalObject.ProcedureName
                , cost: selectedTreatement.originalObject.ProcedureCost
                , note: selectedTreatement.originalObject.DefaultNotes
            });
        }

        function autocompleteChangedCb(originName) {
            if (this.id === "newMuscleTreatement" || this.id === "newJointTreatement") {
                angular.extend($scope[this.id], { treatmentName: originName });

                return void 0;
            }

            angular.extend($scope[this.id][this.$parent.$index], { treatmentName: originName });
        }

        function remove(_case, treatment) {
            if (_case === 'muscle') {
                $scope.muscleTreatements.splice($scope.muscleTreatements.indexOf(treatment), 1);
            }

            if (_case === 'joint') {
                $scope.jointTreatements.splice($scope.jointTreatements.indexOf(treatment), 1);
            }

            return void 0;
        }

        function addTreatement(_case) {
            if (_case === "muscle") {
                $scope.muscleTreatements.push(angular.copy(emptyTreatement));
            }
            else if (_case === "joint") {
                $scope.jointTreatements.push(angular.copy(emptyTreatement));
            }
        }

        function highlightOutput() {
            var finalOutput = {
                "type": "Teeth Problem",
                "name": "Treatment for TMJ &Soft Tissue",
                "data": [{
                    "type": "treatment",
                    "name": "Muscle Tenderness -Right",
                    "data": []
                }, {
                    "type": "treatment",
                    "name": "Joint Tenderness - Rigth",
                    "data": []
                }]
            };

            if ($scope.newMuscleTreatement.treatmentName) {
                finalOutput["data"][0].data.push(angular.copy($scope.newMuscleTreatement));
            }
            angular.forEach($scope.muscleTreatements, function (treatment) {
                if (treatment.treatmentName) {
                    finalOutput["data"][0].data.push(angular.copy(treatment));
                }
            });

            if ($scope.newJointTreatement.treatmentName) {
                finalOutput["data"][1].data.push(angular.copy($scope.newJointTreatement));
            }
            angular.forEach($scope.jointTreatements, function (treatment) {
                if (treatment.treatmentName) {
                    finalOutput["data"][1].data.push(angular.copy(treatment));
                }
            });

            angular.forEach(finalOutput["data"][0].data, function (treatement) {
                delete treatement["showNote"];
                delete treatement["$$hashKey"];
            });
            angular.forEach(finalOutput["data"][1].data, function (treatement) {
                delete treatement["showNote"];
                delete treatement["$$hashKey"];
            });
            $scope.finalOutput = JSON.stringify(finalOutput, undefined, 4);
        }
    }]);
