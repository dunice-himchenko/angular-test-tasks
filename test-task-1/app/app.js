'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute'
    , 'myApp.mainApp'
    , 'angucomplete-alt'
])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/app'});
    }])
;
